/*
 * engine:      hsqldb
 * version:     0.0.1
 * description: initial database structure
 */

/*
 * temporary raw data from xml, and upload journal
 */
create table upload_journal
(
    id         bigserial    not null primary key,
    filename   varchar(100) not null,
    created_at timestamp    not null default now(),
    updated_at timestamp    not null default now(),
    status     varchar(100) not null
);

create table upload_customer
(
    id          bigserial    not null primary key,
    journal_id  bigint       not null references upload_journal,
    inn         varchar(11)  not null unique,
    first_name  varchar(100) not null,
    last_name   varchar(100) not null,
    middle_name varchar(100) not null
);
create index upload_customer_journal_id on upload_customer (journal_id);

create table upload_transaction
(
    id          bigserial      not null primary key,
    journal_id  bigint         not null references upload_journal,
    place       varchar(100)   not null,
    amount      numeric(18, 2) not null,
    currency    varchar(3)     not null,
    card_no     varchar(16)    not null,
    customer_id bigint         not null references upload_customer
);
create index upload_tr_journal_id on upload_transaction (journal_id);
create index upload_tr_customer_id on upload_transaction (customer_id);

create table upload_error
(
    id                bigserial not null primary key,
    journal_id        bigint    not null references upload_journal,
    created_at        timestamp not null default now(),
    exception_class   varchar(255),
    exception_message varchar(255),
    stacktrace        text
);
create index upload_error_journal_id on upload_error (journal_id);
