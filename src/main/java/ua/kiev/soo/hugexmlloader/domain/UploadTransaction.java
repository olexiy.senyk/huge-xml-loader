package ua.kiev.soo.hugexmlloader.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UploadTransaction extends BaseEntity<Long> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "journal_id")
    private UploadJournal journal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private UploadCustomer customer;

    @Column(nullable = false, length = 100)
    private String place;

    @Column(nullable = false, length = 100, scale = 18, precision = 2)
    private BigDecimal amount;

    @Column(nullable = false, length = 3)
    private String currency;

    @Column(nullable = false, length = 16)
    private String cardNo;

    public UploadTransaction(UploadJournal journal) {
        this.journal = journal;
    }
}
