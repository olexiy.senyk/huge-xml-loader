package ua.kiev.soo.hugexmlloader.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UploadCustomer extends BaseEntity<Long> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "journal_id")
    private UploadJournal journal;

    @Column(nullable = false, length = 11, unique = true)
    private String inn;

    @Column(nullable = false, length = 100)
    private String firstName;

    @Column(nullable = false, length = 100)
    private String lastName;

    @Column(nullable = false, length = 100)
    private String middleName;

    public UploadCustomer(UploadJournal journal) {
        this.journal = journal;
    }

//    public UploadCustomer(UploadJournal journal, String inn, String firstName, String lastName, String middleName) {
//        this.journal = journal;
//        this.inn = inn;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.middleName = middleName;
//    }
}
