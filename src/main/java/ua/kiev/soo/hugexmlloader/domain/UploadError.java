package ua.kiev.soo.hugexmlloader.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class UploadError extends BaseEntity<Long> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "journal_id")
    private UploadJournal journal;

    @CreatedDate
    @Column(nullable = false)
    private LocalDateTime createdAt;

    @Column
    private String exceptionClass;

    @Column
    private String exceptionMessage;

    @Column
    private String stacktrace;

    public UploadError(UploadJournal journal) {
        this.journal = journal;
    }

    public UploadError(UploadJournal journal, String exceptionClass, String exceptionMessage, String stacktrace) {
        this.journal = journal;
        this.exceptionClass = exceptionClass;
        this.exceptionMessage = exceptionMessage;
        this.stacktrace = stacktrace;
    }

    @PrePersist
    public void beforePersist() {
        final LocalDateTime entryDate = LocalDateTime.now();
        setCreatedAt(entryDate);
    }
}
