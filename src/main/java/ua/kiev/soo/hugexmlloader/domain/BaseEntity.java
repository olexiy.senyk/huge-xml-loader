package ua.kiev.soo.hugexmlloader.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.Identifiable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class BaseEntity<ID extends Number> implements Identifiable<ID> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private ID id;

    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null || !this.getClass().equals(that.getClass())) {
            return false;
        }
        final BaseEntity thatBE = (BaseEntity) that;
        return this.getId() != null && thatBE.getId() != null && this.getId().equals(thatBE.getId());
    }

    public int hashCode() {
        return getId() == null ? -1 : getId().hashCode();
    }
}
