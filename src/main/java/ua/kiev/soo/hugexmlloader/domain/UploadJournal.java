package ua.kiev.soo.hugexmlloader.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class UploadJournal extends BaseEntity<Long> {

    @Column(nullable = false, length = 100)
    private String filename;

    @CreatedDate
    @Column(nullable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(nullable = false)
    private LocalDateTime updatedAt;

    @Column(nullable = false, length = 100)
    private String status;

    public UploadJournal(String filename, String status) {
        this.filename = filename;
        this.status = status;
    }

    @PrePersist
    public void beforePersist() {
        final LocalDateTime entryDate = LocalDateTime.now();
        setCreatedAt(entryDate);
        setUpdatedAt(entryDate);
        if (getStatus() == null) {
            setStatus("UPLOADING");
        }
    }

    @PreUpdate
    public void beforeUpdate() {
        setUpdatedAt(LocalDateTime.now());
    }
}
