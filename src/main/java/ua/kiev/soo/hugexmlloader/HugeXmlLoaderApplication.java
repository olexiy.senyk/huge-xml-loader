package ua.kiev.soo.hugexmlloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HugeXmlLoaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(HugeXmlLoaderApplication.class, args);
    }

}

