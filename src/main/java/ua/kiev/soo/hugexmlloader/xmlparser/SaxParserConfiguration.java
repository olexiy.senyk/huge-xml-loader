package ua.kiev.soo.hugexmlloader.xmlparser;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

@Configuration
public class SaxParserConfiguration {

    @Bean
    public SAXParserFactory getSaxParserFactory() {
        return SAXParserFactory.newInstance();
    }

    @Bean
    public SAXParser getSaxParser(SAXParserFactory saxParserFactory) throws ParserConfigurationException, SAXException {
        return saxParserFactory.newSAXParser();
    }

}
