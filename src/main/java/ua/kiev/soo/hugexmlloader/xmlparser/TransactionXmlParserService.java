package ua.kiev.soo.hugexmlloader.xmlparser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ua.kiev.soo.hugexmlloader.domain.UploadCustomer;
import ua.kiev.soo.hugexmlloader.domain.UploadJournal;
import ua.kiev.soo.hugexmlloader.domain.UploadTransaction;
import ua.kiev.soo.hugexmlloader.service.UploadCustomerService;
import ua.kiev.soo.hugexmlloader.service.UploadErrorService;
import ua.kiev.soo.hugexmlloader.service.UploadJournalService;
import ua.kiev.soo.hugexmlloader.service.UploadTransactionService;

import javax.xml.parsers.SAXParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

@Component
public class TransactionXmlParserService {

    private final UploadJournalService uploadJournalService;
    private final UploadErrorService uploadErrorService;
    private final UploadCustomerService uploadCustomerService;
    private final UploadTransactionService uploadTransactionService;
    // todo: new parser?
    private final SAXParser parser;

    @Autowired
    public TransactionXmlParserService(UploadJournalService uploadJournalService, UploadErrorService uploadErrorService, UploadCustomerService uploadCustomerService, UploadTransactionService uploadTransactionService, SAXParser parser) {
        this.uploadJournalService = uploadJournalService;
        this.uploadErrorService = uploadErrorService;
        this.uploadCustomerService = uploadCustomerService;
        this.uploadTransactionService = uploadTransactionService;
        this.parser = parser;
    }

    public void parse(File file) throws IOException, SAXException {
        parse(new FileInputStream(file), file.getName());
    }

    public void parse(InputStream file, String filename) throws IOException, SAXException {
        UploadJournal journal = uploadJournalService.save(new UploadJournal(filename, "UPLOADING"));
        parser.parse(file, new TransactionXmlHandler(journal));
    }

    private class TransactionXmlHandler extends DefaultHandler {

        private static final String TRANSACTIONS = "transactions";
        private static final String TRANSACTION = "transaction";
        private static final String TRANSACTION_PLACE = "place";
        private static final String TRANSACTION_AMOUNT = "amount";
        private static final String TRANSACTION_CURRENCY = "currency";
        private static final String TRANSACTION_CARD = "card";
        private static final String CUSTOMER = "client";
        private static final String CUSTOMER_FIRST_NAME = "firstName";
        private static final String CUSTOMER_LAST_NAME = "lastName";
        private static final String CUSTOMER_MIDDLE_NAME = "middleName";
        private static final String CUSTOMER_INN = "inn";

        private final UploadJournal journal;

        private UploadCustomer customer;
        private UploadTransaction transaction;
        private String currentValue;

        private boolean isTransactions;
        private boolean isTransaction;
        private boolean isTransactionPlace;
        private boolean isTransactionAmount;
        private boolean isTransactionCurrency;
        private boolean isTransactionCard;
        private boolean isCustomer;
        private boolean isCustomerFirstName;
        private boolean isCustomerLastName;
        private boolean isCustomerMiddleName;
        private boolean isCustomerInn;

        public TransactionXmlHandler(UploadJournal journal) {
            this.journal = journal;
        }

        @Override
        public void endDocument() throws SAXException {
            journal.setStatus("UPLOADED");
            uploadJournalService.save(journal);
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equalsIgnoreCase(TRANSACTIONS)) {
                isTransactions = true;
            } else if (isTransactions && qName.equalsIgnoreCase(TRANSACTION)) {
                isTransaction = true;
                transaction = new UploadTransaction(journal);
            } else if (isTransaction && qName.equalsIgnoreCase(TRANSACTION_PLACE)) {
                isTransactionPlace = true;
            } else if (isTransaction && qName.equalsIgnoreCase(TRANSACTION_AMOUNT)) {
                isTransactionAmount = true;
            } else if (isTransaction && qName.equalsIgnoreCase(TRANSACTION_CURRENCY)) {
                isTransactionCurrency = true;
            } else if (isTransaction && qName.equalsIgnoreCase(TRANSACTION_CARD)) {
                isTransactionCard = true;
            } else if (isTransaction && qName.equalsIgnoreCase(CUSTOMER)) {
                isCustomer = true;
                customer = new UploadCustomer(journal);
            } else if (isCustomer && qName.equalsIgnoreCase(CUSTOMER_FIRST_NAME)) {
                isCustomerFirstName = true;
            } else if (isCustomer && qName.equalsIgnoreCase(CUSTOMER_LAST_NAME)) {
                isCustomerLastName = true;
            } else if (isCustomer && qName.equalsIgnoreCase(CUSTOMER_MIDDLE_NAME)) {
                isCustomerMiddleName = true;
            } else if (isCustomer && qName.equalsIgnoreCase(CUSTOMER_INN)) {
                isCustomerInn = true;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (qName.equalsIgnoreCase(TRANSACTIONS)) {
                isTransactions = false;
            } else if (isTransactions && qName.equalsIgnoreCase(TRANSACTION)) {
                // todo: add to queue, move to another thread
                uploadTransactionService.save(transaction);
                isTransaction = false;
                transaction = null;
            } else if (isTransactionPlace && qName.equalsIgnoreCase(TRANSACTION_PLACE)) {
                transaction.setPlace(currentValue);
                isTransactionPlace = false;
            } else if (isTransactionAmount && qName.equalsIgnoreCase(TRANSACTION_AMOUNT)) {
                transaction.setAmount(new BigDecimal(currentValue));
                isTransactionAmount = false;
            } else if (isTransactionCurrency && qName.equalsIgnoreCase(TRANSACTION_CURRENCY)) {
                transaction.setCurrency(currentValue);
                isTransactionCurrency = false;
            } else if (isTransactionCard && qName.equalsIgnoreCase(TRANSACTION_CARD)) {
                transaction.setCardNo(currentValue);
                isTransactionCard = false;
            } else if (isCustomer && qName.equalsIgnoreCase(CUSTOMER)) {
                // todo: add to queue, move to another thread
                customer = uploadCustomerService.updateByInn(customer);
                transaction.setCustomer(customer);
                isCustomer = false;
                customer = null;
            } else if (isCustomerFirstName && qName.equalsIgnoreCase(CUSTOMER_FIRST_NAME)) {
                customer.setFirstName(currentValue);
                isCustomerFirstName = false;
            } else if (isCustomerLastName && qName.equalsIgnoreCase(CUSTOMER_LAST_NAME)) {
                customer.setLastName(currentValue);
                isCustomerLastName = false;
            } else if (isCustomerMiddleName && qName.equalsIgnoreCase(CUSTOMER_MIDDLE_NAME)) {
                customer.setMiddleName(currentValue);
                isCustomerMiddleName = false;
            } else if (isCustomerInn && qName.equalsIgnoreCase(CUSTOMER_INN)) {
                customer.setInn(currentValue);
                isCustomerInn = false;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (isTransactionPlace ||
                    isTransactionAmount ||
                    isTransactionCurrency ||
                    isTransactionCard ||
                    isCustomerFirstName ||
                    isCustomerLastName ||
                    isCustomerMiddleName ||
                    isCustomerInn) {
                currentValue = new String(ch, start, length);
            }
        }

        @ExceptionHandler(SAXException.class)
        public void handleSAXException(SAXException ex) {
            uploadErrorService.save(journal, ex);
        }
    }

}
