package ua.kiev.soo.hugexmlloader.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.kiev.soo.hugexmlloader.domain.UploadError;

@Repository
public interface UploadErrorRepository extends JpaRepository<UploadError, Long> {
}
