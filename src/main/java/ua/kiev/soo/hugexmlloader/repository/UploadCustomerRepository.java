package ua.kiev.soo.hugexmlloader.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.kiev.soo.hugexmlloader.domain.UploadCustomer;

import java.util.Optional;

@Repository
public interface UploadCustomerRepository extends JpaRepository<UploadCustomer, Long> {

    Optional<UploadCustomer> findByInn(String inn);

}
