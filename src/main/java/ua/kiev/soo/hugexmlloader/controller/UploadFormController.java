package ua.kiev.soo.hugexmlloader.controller;

import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xml.sax.SAXException;
import ua.kiev.soo.hugexmlloader.xmlparser.TransactionXmlParserService;

import java.io.IOException;

@Controller
@RequestMapping("/")
public class UploadFormController {

    private final TransactionXmlParserService parserService;

    @Autowired
    public UploadFormController(TransactionXmlParserService parserService) {
        this.parserService = parserService;
    }

    @GetMapping("/")
    public String uploadedForm() {
        return "uploadForm";
    }

    @PostMapping
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) throws FileUploadException, SAXException {
        try {
            if (file.isEmpty()) {
                throw new FileUploadException("Failed to upload empty file " + file.getOriginalFilename());
            }
            parserService.parse(file.getInputStream(), file.getOriginalFilename());
        } catch (IOException e) {
            throw new FileUploadException("Failed to upload empty file " + file.getOriginalFilename());
        }

        redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + file.getOriginalFilename() + "!");
        return "redirect:/";
    }

}
