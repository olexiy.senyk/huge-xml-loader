package ua.kiev.soo.hugexmlloader;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ua.kiev.soo.hugexmlloader.xmlparser.TransactionXmlParserService;

import java.io.FileInputStream;
import java.io.InputStream;

@Profile("test")
@Component
public class TestXmlCommandLineRunner implements CommandLineRunner {

    private final TransactionXmlParserService parserService;

    public TestXmlCommandLineRunner(TransactionXmlParserService parserService) {
        this.parserService = parserService;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println();
        try (InputStream fileInputStream = new FileInputStream("test.xml")) {
            parserService.parse(fileInputStream, "test.xml");
        }
    }
}
