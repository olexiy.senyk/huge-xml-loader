package ua.kiev.soo.hugexmlloader.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.kiev.soo.hugexmlloader.domain.UploadJournal;
import ua.kiev.soo.hugexmlloader.repository.UploadJournalRepository;

@Service
public class UploadJournalService extends BaseEntityService<Long, UploadJournal, UploadJournalRepository> {
    @Autowired
    public UploadJournalService(UploadJournalRepository repository) {
        super(repository);
    }
}
