package ua.kiev.soo.hugexmlloader.service;

import org.springframework.stereotype.Service;
import ua.kiev.soo.hugexmlloader.domain.UploadTransaction;
import ua.kiev.soo.hugexmlloader.repository.UploadTransactionRepository;

@Service
public class UploadTransactionService extends BaseEntityService<Long, UploadTransaction, UploadTransactionRepository> {
    public UploadTransactionService(UploadTransactionRepository repository) {
        super(repository);
    }
}
