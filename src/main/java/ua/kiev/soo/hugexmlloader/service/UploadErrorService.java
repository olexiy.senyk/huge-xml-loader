package ua.kiev.soo.hugexmlloader.service;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.stereotype.Service;
import ua.kiev.soo.hugexmlloader.domain.UploadError;
import ua.kiev.soo.hugexmlloader.domain.UploadJournal;
import ua.kiev.soo.hugexmlloader.repository.UploadErrorRepository;

@Service
public class UploadErrorService {

    private final UploadErrorRepository repository;

    public UploadErrorService(UploadErrorRepository repository) {
        this.repository = repository;
    }

    public UploadError save(UploadJournal journal, Throwable ex) {
        UploadError uploadError = new UploadError(
                journal,
                ex.getClass().getCanonicalName(),
                ex.getMessage(),
                ExceptionUtils.getStackTrace(ex));
        return repository.save(uploadError);
    }

}
