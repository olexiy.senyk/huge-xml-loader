package ua.kiev.soo.hugexmlloader.service;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.kiev.soo.hugexmlloader.domain.BaseEntity;

import java.util.List;

public class BaseEntityService<ID extends Number, E extends BaseEntity<ID>, R extends JpaRepository<E, ID>> {

    protected final R repository;

    public BaseEntityService(R repository) {
        this.repository = repository;
    }

    public List<E> findAll() {
        return repository.findAll();
    }

    public E findOne(ID id) {
        return repository.findById(id).orElse(null);
    }

    public E save(E prototype) {
        return repository.save(prototype);
    }
}
