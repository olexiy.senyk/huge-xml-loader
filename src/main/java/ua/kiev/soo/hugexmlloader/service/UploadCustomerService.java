package ua.kiev.soo.hugexmlloader.service;

import org.springframework.stereotype.Service;
import ua.kiev.soo.hugexmlloader.domain.UploadCustomer;
import ua.kiev.soo.hugexmlloader.repository.UploadCustomerRepository;

import java.util.Optional;

@Service
public class UploadCustomerService extends BaseEntityService<Long, UploadCustomer, UploadCustomerRepository> {

    public UploadCustomerService(UploadCustomerRepository repository) {
        super(repository);
    }

    public Optional<UploadCustomer> findByInn(String inn) {
        return repository.findByInn(inn);
    }

    public UploadCustomer updateByInn(UploadCustomer customer) {
        return findByInn(customer.getInn()).orElseGet(() -> save(customer));
    }
}
