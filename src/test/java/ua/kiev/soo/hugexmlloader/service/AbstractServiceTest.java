package ua.kiev.soo.hugexmlloader.service;

import ua.kiev.soo.hugexmlloader.AbstractHugeXmlLoaderApplicationTests;
import ua.kiev.soo.hugexmlloader.domain.BaseEntity;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public abstract class AbstractServiceTest<E extends BaseEntity> extends AbstractHugeXmlLoaderApplicationTests {

    void testList(List<E> list, int expectedSize) {
        assertNotNull("FAILURE. Expected not null list", list);
        assertEquals("FAILURE. Expected list size", expectedSize, list.size());
    }

}
