package ua.kiev.soo.hugexmlloader.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.kiev.soo.hugexmlloader.domain.UploadCustomer;
import ua.kiev.soo.hugexmlloader.domain.UploadJournal;
import ua.kiev.soo.hugexmlloader.domain.UploadTransaction;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
@Transactional
public class UploadTransactionServiceTest extends AbstractServiceTest<UploadTransaction> {

    private static final int INITIAL_LIST_SIZE = 12;
    private static final long INITIAL_JOURNAL_ID = 1;
    private static final long INITIAL_CUSTOMER_ID = 1;
    private static final long INITIAL_ID = 1;

    private static final String PLACE = "A PLACE 1";
    private static final BigDecimal AMOUNT = BigDecimal.valueOf(10.01);
    private static final String CURRENCY = "UAH";
    private static final String CARD_NO = "123456****1234";
    private static final String TEST_CARD_NO = "987654****3210";

    private UploadJournal journal;
    private UploadCustomer customer;

    @Autowired
    private UploadJournalService journalService;

    @Autowired
    private UploadCustomerService customerService;

    @Autowired
    private UploadTransactionService service;

    @Before
    public void setUp() {
        journal = journalService.findOne(INITIAL_JOURNAL_ID);
        customer = customerService.findOne(INITIAL_CUSTOMER_ID);
    }

    @After
    public void cleanUp() {
        journal = null;
        customer = null;
    }

    @Test
    public void findAll() {
        List<UploadTransaction> list = service.findAll();
        testList(list, INITIAL_LIST_SIZE);
    }

    @Test
    public void findOne() {
        UploadTransaction entity = service.findOne(INITIAL_ID);
        assertNotNull("FAILURE. Expected not null entity", entity);
    }

    @Test
    public void findNotExisting() {
        UploadTransaction entity = service.findOne(Long.MAX_VALUE);
        assertNull("FAILURE. Expected null entity", entity);
    }

    @Test
    public void create() {
        UploadTransaction entity = new UploadTransaction(journal, customer, PLACE, AMOUNT, CURRENCY, CARD_NO);
        UploadTransaction createdEntity = service.save(entity);

        TransactionDto expectedDto = TransactionDto.defaultExample();
        expectedDto.setId((long) INITIAL_LIST_SIZE + 1);

        TransactionDto.of(createdEntity).assertWith(expectedDto);

        List<UploadTransaction> list = service.findAll();
        testList(list, INITIAL_LIST_SIZE + 1);
    }

    @Test
    public void update() {
        UploadTransaction entity = service.findOne(INITIAL_ID);
        assertNotNull("FAILURE. Expected not null entity", entity);
        entity.setCardNo(TEST_CARD_NO);
        UploadTransaction updatedEntity = service.save(entity);

        assertNotNull("FAILURE. Expected not null updated entity", entity);
        assertEquals("FAILURE. Expected cardNo attribute match", TEST_CARD_NO, updatedEntity.getCardNo());

        List<UploadTransaction> list = service.findAll();
        testList(list, INITIAL_LIST_SIZE);
    }


    @Data
    @AllArgsConstructor
    private static class TransactionDto {
        private Long id;
        private Long journalId;
        private Long customerId;
        private String place;
        private BigDecimal amount;
        private String currency;
        private String cardNo;

        private TransactionDto(UploadTransaction example) {
            this.id = example.getId();
            this.journalId = example.getJournal().getId();
            this.customerId = example.getJournal().getId();
            this.place = example.getPlace();
            this.amount = example.getAmount();
            this.currency = example.getCurrency();
            this.cardNo = example.getCardNo();
        }

        static TransactionDto of(UploadTransaction example) {
            return new TransactionDto(example);
        }

        static TransactionDto defaultExample() {
            return new TransactionDto(
                    INITIAL_ID,
                    INITIAL_JOURNAL_ID,
                    INITIAL_CUSTOMER_ID,
                    PLACE,
                    AMOUNT,
                    CURRENCY,
                    CARD_NO);
        }

        void assertWith(TransactionDto expected) {
            assertNotNull("FAILURE. Expected not null ID", this.getId());
            assertEquals("FAILURE. Expected id attribute match", expected.getId(), this.getId());
            assertNotNull("FAILURE. Expected not null journal attribute", this.getJournalId());
            assertEquals("FAILURE. Expected journal attribute match", expected.getJournalId(), this.getJournalId());
            assertNotNull("FAILURE. Expected not null customer attribute", this.getCustomerId());
            assertEquals("FAILURE. Expected customer attribute match", expected.getCustomerId(), this.getCustomerId());
            assertEquals("FAILURE. Expected  attribute match", expected.getPlace(), this.getPlace());
            assertEquals("FAILURE. Expected  attribute match", expected.getAmount(), this.getAmount());
            assertEquals("FAILURE. Expected  attribute match", expected.getCurrency(), this.getCurrency());
            assertEquals("FAILURE. Expected  attribute match", expected.getCardNo(), this.getCardNo());
        }

        void assertWith(UploadTransaction expected) {
            assertWith(new TransactionDto(expected));
        }
    }
}
