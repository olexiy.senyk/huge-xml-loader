package ua.kiev.soo.hugexmlloader.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.kiev.soo.hugexmlloader.domain.UploadCustomer;
import ua.kiev.soo.hugexmlloader.domain.UploadJournal;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
@Transactional
public class UploadCustomerServiceTest extends AbstractServiceTest<UploadCustomer> {

    private static final long INITIAL_JOURNAL_ID = 1;
    private static final long INITIAL_ID = 1;
    private static final String INITIAL_INN = "1234567890";
    private static final String INITIAL_FIRST_NAME = "Ivan";
    private static final String INITIAL_LAST_NAME = "Ivanoff";
    private static final String INITIAL_MIDDLE_NAME = "Ivanoff";
    private static final int INITIAL_LIST_SIZE = 3;

    private static final String TEST_STRING = "created or updated by by junit test";
    private static final String TEST_INN = "9234567890";

    private UploadJournal journal;

    @Autowired
    private UploadJournalService journalService;

    @Autowired
    private UploadCustomerService service;

    @Before
    public void setUp() {
        journal = journalService.findOne(INITIAL_JOURNAL_ID);
    }

    @After
    public void cleanUp() {
        journal = null;
    }

    @Test
    public void findAll() {
        List<UploadCustomer> list = service.findAll();
        testList(list, INITIAL_LIST_SIZE);
    }

    @Test
    public void findByInn() {
        UploadCustomer entity = service.findByInn(INITIAL_INN).orElse(null);
        assertNotNull(entity);
        CustomerDto.of(entity).assertWith(CustomerDto.defaultExample());
    }

    @Test
    public void findNotExisting() {
        UploadCustomer entity = service.findOne(Long.MAX_VALUE);
        assertNull("FAILURE. Expected null entity", entity);

        entity = service.findByInn(TEST_INN).orElse(null);
        assertNull("FAILURE. Expected null entity", entity);
    }

    @Test
    public void create() {
        UploadCustomer entity = new UploadCustomer(journal, TEST_INN, TEST_STRING, TEST_STRING, TEST_STRING);
        UploadCustomer createdEntity = service.save(entity);
        CustomerDto.of(createdEntity).assertWith(entity);

        List<UploadCustomer> list = service.findAll();
        testList(list, INITIAL_LIST_SIZE + 1);
    }

    @Test
    public void update() {
        UploadCustomer entity = service.findOne(INITIAL_ID);
        assertNotNull("FAILURE. Expected not null entity", entity);
        entity.setFirstName(TEST_STRING);
        UploadCustomer updatedEntity = service.save(entity);

        assertNotNull("FAILURE. Expected not null updated entity", entity);
        assertEquals("FAILURE. Expected first name attribute match", TEST_STRING, updatedEntity.getFirstName());

        List<UploadCustomer> list = service.findAll();
        testList(list, INITIAL_LIST_SIZE);
    }

    @Data
    @AllArgsConstructor
    private static class CustomerDto {
        private Long id;
        private Long journalId;
        private String inn;
        private String firstName;
        private String lastName;
        private String middleName;

        private CustomerDto(UploadCustomer example) {
            this.id = example.getId();
            this.journalId = example.getJournal().getId();
            this.inn = example.getInn();
            this.firstName = example.getFirstName();
            this.lastName = example.getLastName();
            this.middleName = example.getMiddleName();
        }

        static CustomerDto of(UploadCustomer example) {
            return new CustomerDto(example);
        }

        static CustomerDto defaultExample() {
            return new CustomerDto(
                    INITIAL_ID,
                    INITIAL_JOURNAL_ID,
                    INITIAL_INN,
                    INITIAL_FIRST_NAME,
                    INITIAL_LAST_NAME,
                    INITIAL_MIDDLE_NAME);
        }

        void assertWith(CustomerDto expected) {
            assertNotNull("FAILURE. Expected not null ID", this.getId());
            assertEquals("FAILURE. Expected id attribute match", expected.getId(), this.getId());
            assertNotNull("FAILURE. Expected not null journal attribute", this.getJournalId());
            assertEquals("FAILURE. Expected journal attribute match", expected.getJournalId(), this.getJournalId());
            assertEquals("FAILURE. Expected firstName attribute match", expected.getFirstName(), this.getFirstName());
            assertEquals("FAILURE. Expected lastName attribute match", expected.getLastName(), this.getLastName());
            assertEquals("FAILURE. Expected middleName attribute match", expected.getMiddleName(), this.getMiddleName());
            assertEquals("FAILURE. Expected inn attribute match", expected.getInn(), this.getInn());
        }

        void assertWith(UploadCustomer expected) {
            assertWith(new CustomerDto(expected));
        }
    }

}
