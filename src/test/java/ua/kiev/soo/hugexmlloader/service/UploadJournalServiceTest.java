package ua.kiev.soo.hugexmlloader.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.kiev.soo.hugexmlloader.domain.UploadJournal;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
@Transactional
public class UploadJournalServiceTest extends AbstractServiceTest<UploadJournal> {

    private static final long INITIAL_ID = 1;
    private static final long CREATED_ID = 2;
    private static final int INITIAL_LIST_SIZE = 1;

    private static final String CREATED_FILENAME = "created by junit test";
    private static final String CREATED_STATUS = "CREATED TEST";
    private static final String UPDATED_STATUS = "UPDATED TEST";

    @Autowired
    private UploadJournalService service;

    @Test
    public void findAll() {
        List<UploadJournal> list = service.findAll();
        testList(list, INITIAL_LIST_SIZE);
    }

    @Test
    public void findNotExisting() {
        UploadJournal entity = service.findOne(Long.MAX_VALUE);
        assertNull("FAILURE. Expected null entity", entity);
    }

    @Test
    public void create() {
        UploadJournal entity = new UploadJournal(CREATED_FILENAME, CREATED_STATUS);
        UploadJournal createdEntity = service.save(entity);

        assertNotNull("FAILURE. Expected not null entity", createdEntity);
        assertNotNull("FAILURE. Expected not null ID", createdEntity.getId());
        assertEquals("FAILURE. Expected id attribute match", CREATED_ID, createdEntity.getId().longValue());
        assertEquals("FAILURE. Expected filename attribute match", CREATED_FILENAME, createdEntity.getFilename());
        assertEquals("FAILURE. Expected status attribute match", CREATED_STATUS, createdEntity.getStatus());
        assertNotNull("FAILURE. Expected not null createdAt", createdEntity.getCreatedAt());
        assertNotNull("FAILURE. Expected not null updatedAt", createdEntity.getUpdatedAt());

        List<UploadJournal> list = service.findAll();
        testList(list, INITIAL_LIST_SIZE + 1);
    }

    @Test
    public void update() {
        UploadJournal entity = service.findOne(INITIAL_ID);
        assertNotNull("FAILURE. Expected not null entity", entity);

        entity.setStatus(UPDATED_STATUS);
        UploadJournal updatedEntity = service.save(entity);

        assertNotNull("FAILURE. Expected not null updated entity", entity);
        assertEquals("FAILURE. Expected status attribute match", UPDATED_STATUS, updatedEntity.getStatus());

        List<UploadJournal> list = service.findAll();
        testList(list, INITIAL_LIST_SIZE);
    }

}
